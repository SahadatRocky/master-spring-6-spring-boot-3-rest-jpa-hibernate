package com.topics.example.service;

import com.topics.example.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

@Slf4j
@RequestScope
//@SessionScope
//@ApplicationScope
@Service
public class UserService {

    public UserService(){
        System.out.println("user service bean initialized");
    }
    public String getUSerName(){
        log.info("log print find user");
        User user = new User();
        user.setUserName("jerry 1");

        return user.getUserName();
    }

    public String getId(String id){
        log.info("log print find user id");
        return "ID :" + id;
    }
}
