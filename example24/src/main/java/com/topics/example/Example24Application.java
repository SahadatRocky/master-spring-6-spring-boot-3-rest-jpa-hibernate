package com.topics.example;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Example24Application {

	public static void main(String[] args) {

		SpringApplication.run(Example24Application.class, args);
	}

}
