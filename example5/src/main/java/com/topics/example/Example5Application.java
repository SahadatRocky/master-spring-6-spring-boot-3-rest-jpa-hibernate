package com.topics.example;

import com.topics.example.beans.Vehicle;
import com.topics.example.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Example5Application {

	public static void main(String[] args) {

		SpringApplication.run(Example5Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);

		Vehicle vehicle = context.getBean(Vehicle.class);
		System.out.println("Component Vehicle name from Spring Context is: " + vehicle.getName());
		vehicle.printHello();

	}

}
