package com.topics.example;
import com.topics.example.beans.Vehicle;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;


@SpringBootApplication
public class Example8Application {

	public static void main(String[] args) {

		SpringApplication.run(Example8Application.class, args);

		var context = new ClassPathXmlApplicationContext("beans.xml");
		Vehicle vehicle = context.getBean(Vehicle.class);
		System.out.println("Vehicle name from Spring Context is: " + vehicle.getName());
	}

}
