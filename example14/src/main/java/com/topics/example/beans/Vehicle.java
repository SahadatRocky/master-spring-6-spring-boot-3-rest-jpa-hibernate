package com.topics.example.beans;

import com.topics.example.services.VehicleServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("vehicleBean")
public class Vehicle {
    private String name = "honda";
    private final VehicleServices vehicleServices;

    @Autowired
    public Vehicle(VehicleServices vehicleServices){

        System.out.println("Vehicle bean created by Spring");
        this.vehicleServices = vehicleServices;
    }
    public String getName() {
        return name;
    }

    public VehicleServices getVehicleServices() {
        return vehicleServices;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void printHello(){
        System.out.println(
                "Printing Hello from Component Vehicle Bean");
    }
}
