package com.topics.example.config;

import com.topics.example.beans.Vehicle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/*
Spring @Configuration annotation is a part of spring core framework.
Spring Configuration annotation indicates that class has @Bean definition method.
so spring container can process the class and generate spring Beans to be used in the application.

*/
@Configuration
public class ProjectConfig {

    /*
    @Bean annotation spring knows that it need to call this method
    when it initializes its context and return value to the context.
    */

    @Bean(name="audiVehicle")
    Vehicle vehicle1(){
        Vehicle veh = new Vehicle();
        veh.setName("Audi 8");
        return veh;
    }

    @Bean(value="hondaVehicle")
    Vehicle vehicle2() {
        var veh = new Vehicle();
        veh.setName("Honda");
        return veh;
    }

    @Primary
    @Bean("ferrariVehicle")
    Vehicle vehicle3() {
        var veh = new Vehicle();
        veh.setName("Ferrari");
        return veh;
    }

}
