package com.topics.example;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Example20Application {

	public static void main(String[] args) {

		SpringApplication.run(Example20Application.class, args);
	}

}
