package com.topics.example.config;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "com.topics.example.implementation", "com.topics.example.services"})
@ComponentScan(basePackageClasses = {com.topics.example.beans.Vehicle.class, com.topics.example.beans.Person.class})
public class ProjectConfig {

}
