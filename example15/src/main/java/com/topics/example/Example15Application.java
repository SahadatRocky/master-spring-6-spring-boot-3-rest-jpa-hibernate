package com.topics.example;
import com.topics.example.beans.Person;
import com.topics.example.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@SpringBootApplication
public class Example15Application {

	public static void main(String[] args) {

		SpringApplication.run(Example15Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);
		System.out.println("Before retrieving the Person bean from the Spring Context");
		Person person = context.getBean(Person.class);
		System.out.println("After retrieving the Person bean from the Spring Context");

	}

}
