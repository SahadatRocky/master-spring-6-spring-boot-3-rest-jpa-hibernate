package com.topics.example.beans;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.springframework.stereotype.Component;

@Component
public class Vehicle {
    private String name;

    @PostConstruct
    public void inital(){
        this.name = "honda";
    }

    @PreDestroy
    public void destory(){
        System.out.println("Destroying Vehicle Bean");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void printHello(){
        System.out.println(
                "Printing Hello from Component Vehicle Bean");
    }
}
