package com.topics.example;

import com.topics.example.beans.Vehicle;
import com.topics.example.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Example2Application {

	public static void main(String[] args) {

		SpringApplication.run(Example2Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);
		// Vehicle veh = context.getBean(Vehicle.class);
		Vehicle veh = context.getBean("vehicle1",Vehicle.class);
		System.out.println("Vehicle name from Spring Context is: " + veh.getName());

	}

}
