package com.topics.example.config;

import com.topics.example.beans.Person;
import com.topics.example.beans.Vehicle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProjectConfig {

    @Bean
    public Vehicle vehicle(){
        Vehicle veh = new Vehicle();
        veh.setName("Toyota");
        return veh;
    }

    @Bean
    public Person person(Vehicle vehicle){
        Person p = new Person();
        p.setName("Rocky");
        p.setVehicle(vehicle);
        return p;
    }

}
