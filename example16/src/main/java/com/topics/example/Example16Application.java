package com.topics.example;
import com.topics.example.beans.Person;
import com.topics.example.config.ProjectConfig;
import com.topics.example.services.VehicleServices;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@SpringBootApplication
public class Example16Application {

	public static void main(String[] args) {

		SpringApplication.run(Example16Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);
		VehicleServices vehicleServices1 = context.getBean(VehicleServices.class);
		VehicleServices vehicleServices2 = context.getBean("vehicleServices",VehicleServices.class);
		System.out.println("Hashcode of the object vehicleServices1 : " +vehicleServices1.hashCode());
		System.out.println("Hashcode of the object vehicleServices2 : " +vehicleServices2.hashCode());
		if(vehicleServices1==vehicleServices2){
			System.out.println("VehicleServices bean is a singleton scoped bean");
		}else{
			System.out.println("VehicleServices bean is a prototype scoped bean");
		}

	}

}
