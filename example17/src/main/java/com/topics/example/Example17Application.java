package com.topics.example;
import com.topics.example.beans.ShoppingCart;
import com.topics.example.config.BeanConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@SpringBootApplication
public class Example17Application {

	public static void main(String[] args) {

		SpringApplication.run(Example17Application.class, args);
		ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfig.class);
		ShoppingCart cart = context.getBean(ShoppingCart.class);
		cart.checkout("CANCELED");
	}

}
