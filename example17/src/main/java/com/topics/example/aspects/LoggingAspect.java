package com.topics.example.aspects;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;

@Aspect
@Component
public class LoggingAspect {

    @Before("execution(* com.topics.example.beans.ShoppingCart.checkout(..))")
    public void beforeLogger(JoinPoint jp){
        System.out.println(jp.getSignature());
        String arg = jp.getArgs()[0].toString();

        System.out.println("Before Loggers ...with args " +arg);
    }

    @After("execution(* com.topics.example.beans.ShoppingCart.checkout(..))")
    public void afterLogger(){
        System.out.println("after Loggers ...");
    }

//    @Pointcut("execution(* com.topics.example.beans.ShoppingCart.*(..))")
//    public void afterReturningPointCut(){
//    }

    @AfterReturning(
            value = "execution(* com.topics.example.beans.ShoppingCart.quantity())",
            returning = "retVal")
    public void afterReturning(JoinPoint joinPoint, Integer retVal) {
        System.out.println(joinPoint.getSignature());
        System.out.println("After Returning..." + retVal);
    }

    @AfterThrowing(value = "execution(* com.topics.example.beans.ShoppingCart.checkout(..))",throwing = "ex")
    public void logException(JoinPoint joinPoint, Exception ex) {
        System.out.println(joinPoint.getSignature());
        System.out.println("An exception thrown with the help of" +
                " @AfterThrowing which happened due to : "+ex.getMessage());
    }

    @Around("execution(* com.topics.example.beans.ShoppingCart.checkout(..))")
    public void log(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println(joinPoint.getSignature().toString() + " method execution start");
        Instant start = Instant.now();
        joinPoint.proceed();
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println("Time took to execute the method : "+timeElapsed);
        System.out.println(joinPoint.getSignature().toString() + " method execution end");
    }
}
