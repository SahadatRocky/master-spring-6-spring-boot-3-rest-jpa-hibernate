package com.topics.example.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AuthenticationAspect {


    @Pointcut("execution(* com.topics.example.beans.ShoppingCart.*(..))")
    public void authenticationPointCut(){

    }

    @Pointcut("execution(* com.topics.example.beans.ShoppingCart.*(..))")
    public void authorizationPointCut(){

    }


    @Before("authenticationPointCut() && authorizationPointCut()")
    public void Authenticate(){
        System.out.println("Authenticating the request.");
    }
}
