package com.topics.example.beans;
import org.springframework.stereotype.Component;

@Component
public class ShoppingCart {

    public void checkout(String status){
        System.out.println("Checkout method from shopping card calling ...");
    }

    public Integer quantity(){
        System.out.println("quantity..");
        return 2;
     }
}
