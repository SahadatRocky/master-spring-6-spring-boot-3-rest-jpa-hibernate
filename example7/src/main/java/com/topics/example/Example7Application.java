package com.topics.example;

import com.topics.example.beans.Vehicle;
import com.topics.example.config.ProjectConfig;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Random;
import java.util.function.Supplier;

@SpringBootApplication
public class Example7Application {

	public static void main(String[] args) {

		SpringApplication.run(Example7Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);

		Vehicle volkswagen = new Vehicle();
		volkswagen.setName("Volkswagen");
		Supplier<Vehicle> volkswagonSupplier = () -> volkswagen;

		Supplier<Vehicle> audiSupplier = () -> {
			Vehicle audi = new Vehicle();
			audi.setName("Audi");
			return audi;
		};

		Random random = new Random();
		int randomNumber = random.nextInt(10);

		System.out.println("randomNumber = " + randomNumber);

		if(randomNumber % 2 == 0){
			context.registerBean("volkswagen", Vehicle.class, volkswagonSupplier);
		}
		else{
			context.registerBean("audi", Vehicle.class, audiSupplier);
		}


		Vehicle volksVehicle = null;
		Vehicle audiVehicle = null;
		try {
			volksVehicle = context.getBean("volkswagen",Vehicle.class);
		}catch (NoSuchBeanDefinitionException noSuchBeanDefinitionException){
			System.out.println("Error while creating Volkswagen vehicle");
		}
		try {
			audiVehicle = context.getBean("audi",Vehicle.class);
		}catch (NoSuchBeanDefinitionException noSuchBeanDefinitionException){
			System.out.println("Error while creating Audi vehicle");
		}

		if(null != volksVehicle){
			System.out.println("Programming Vehicle name from Spring Context is: " + volksVehicle.getName());
		}else{
			System.out.println("Programming Vehicle name from Spring Context is:" + audiVehicle.getName());
		}

	}

}
