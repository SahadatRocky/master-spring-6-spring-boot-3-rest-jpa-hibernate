package com.topics.example.config;

import com.topics.example.beans.Vehicle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/*
Spring @Configuration annotation is a part of spring core framework.
Spring Configuration annotation indicates that class has @Bean definition method.
so spring container can process the class and generate spring Beans to be used in the application.

*/
@Configuration
@ComponentScan(basePackages = "com.topics.example.beans")
public class ProjectConfig {

}
