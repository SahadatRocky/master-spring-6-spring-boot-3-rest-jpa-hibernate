package com.topics.example;
import com.topics.example.beans.Person;
import com.topics.example.beans.Vehicle;
import com.topics.example.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@SpringBootApplication
public class Example12Application {

	public static void main(String[] args) {

		SpringApplication.run(Example12Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);
		Person person = context.getBean(Person.class);
		System.out.println("Person name from Spring Context is: " + person.getName());
		System.out.println("Vehicle that Person own is: " + person.getVehicle().getName());
	}

}
