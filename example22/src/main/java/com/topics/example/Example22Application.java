package com.topics.example;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Example22Application {

	public static void main(String[] args) {

		SpringApplication.run(Example22Application.class, args);
	}

}
