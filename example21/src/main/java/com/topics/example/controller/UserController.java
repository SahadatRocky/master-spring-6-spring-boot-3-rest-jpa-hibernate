package com.topics.example.controller;

import com.topics.example.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private final UserService userService;
    public UserController(UserService userService){
        this.userService = userService;
    }

    @GetMapping("/user")
    public String getUserName(){
        return this.userService.getUSerName();
    }
}
