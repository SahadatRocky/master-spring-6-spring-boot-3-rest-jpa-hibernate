package com.topics.example;

import com.topics.example.beans.Vehicle;
import com.topics.example.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Example3Application {

	public static void main(String[] args) {

		SpringApplication.run(Example3Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);
		Vehicle veh1 = context.getBean("audiVehicle",Vehicle.class);
		System.out.println("Vehicle name from Spring Context is: " + veh1.getName());

		Vehicle veh2 = context.getBean("hondaVehicle",Vehicle.class);
		System.out.println("Vehicle name from Spring Context is: " + veh2.getName());

		Vehicle veh3 = context.getBean("ferrariVehicle",Vehicle.class);
		System.out.println("Vehicle name from Spring Context is: " + veh3.getName());

	}

}
