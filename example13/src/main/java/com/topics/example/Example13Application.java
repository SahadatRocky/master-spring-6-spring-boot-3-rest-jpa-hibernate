package com.topics.example;
import com.topics.example.beans.Person;
import com.topics.example.beans.Vehicle;
import com.topics.example.config.ProjectConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


@SpringBootApplication
public class Example13Application {

	public static void main(String[] args) {

		SpringApplication.run(Example13Application.class, args);

		var context = new AnnotationConfigApplicationContext(ProjectConfig.class);
		String[] persons = context.getBeanNamesForType(Person.class);
		Person person = context.getBean(Person.class);
		String[] names = context.getBeanNamesForType(Vehicle.class);
		person.getVehicle().getVehicleServices().playMusic();
		person.getVehicle().getVehicleServices().moveVehicle();
	}

}
