package com.topics.example.implementation;

import com.topics.example.interfaces.Speakers;
import org.springframework.stereotype.Component;

@Component
public class BoseSpeakers implements Speakers {
    @Override
    public String makeSound() {
        return "playing music with bose Speakers";
    }
}
