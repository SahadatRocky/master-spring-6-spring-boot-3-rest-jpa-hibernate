package com.topics.example.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String displayHome(Model model){
        model.addAttribute("username", "Rocky");
        return "home.html";
    }
}
